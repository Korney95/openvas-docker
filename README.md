
# Build by GmbH official manuals: https://github.com/greenbone/



## ``` Variables to adjust: ```

### ``Put this variables in your .env file``

|  Variable | Value |
|-------------|---------
|  ``USERNAME`` | web user (default admin)  |
|  ``PASSWORD`` | passwdord for web (default admin) |
|  ``TZ`` | set timezone (default UTC) |

``Database user``: postgres
``database``: gvmd 

``default url``: https://localhost:9392/
